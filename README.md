# BSE(OUSL) schedule remainder base on date wise activity diary #

> This project will set up your google calendar according to your subjects activities, You can configure   subjects and remainder time periods. This is a hackable project you can modify this as your desire and redistribute.

 > This project is a command line application which based on PHP language. 

** Important this project contain  2016-2017 date wise activity diary, you can add new activity diary whenever you need as a pull request

** Important! Here we have put primary calender to event create. When ever you run this that primary calender will be cleaned your existing appointments will be lost to prevent that create an another calender.     

#Prerequisites#

To run this, you'll need:

    I. PHP 5.3.2 or greater with the command-line interface (CLI) and JSON extension installed.
    II. The Composer dependency management tool.
    III. Access to the internet and a web browser.
    IV. A Google account with Google Calendar enabled.

### Installation ###

#### 01. PHP installation ####
>Installation guidelines  
>* [Manual](http://php.net/manual/en/install.php)
>* Or you can use any methords to install PHP

#### 02. Composer installation ####
>Installation guidelines  
>* [Linux|Unix|OSX](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx) Or 
>* [Windows](https://getcomposer.org/doc/00-intro.md#installation-windows)

#### 03. Dependency installation ####
>This project has few dependencies, to install that dependencies go to project folder in your PC and type following command. 
   
    $ composer install 

##### Dependency #####
> 01. [Sympony DomCrawler](http://symfony.com/doc/current/components/dom_crawler.html) we used this package to read through the activity diary. (initially, we converted activity diary as HTML document then use this component to read them)
02. [Google API Client - PHP](https://developers.google.com/api-client-library/php/) to access your google calendar

### How to run this ? ###
Setup your subjects in $enrolments  array in runner.php file

    $enrolments = ['ECI5266','ECI6260','ECY6489','MPZ5160'];

Run the project using the following command:

    $ php runner.php

>The first time you run the project, it will prompt you to authorize access:

>Browse to the provided URL in your web browser.

>If you are not already logged into your Google account, you will be prompted to log in. If you are logged into multiple Google accounts, you will be asked to select one account to use for the authorization.
Click the Accept button.
Copy the code you're given, paste it into the command-line prompt, and press Enter.

Then you will have your course activities in your google calendar.
