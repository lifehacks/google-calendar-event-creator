<?php
date_default_timezone_set('Asia/Colombo');
require_once("./vendor/autoload.php");
require_once("./calender.php");

use Symfony\Component\DomCrawler\Crawler;

$diary_html = file_get_contents('date_wise_diary.html');
$crawler = new Crawler($diary_html);

$schedules = [];

$enrolments = ['MPZ5160','ECX5263','MPJ5263','ECI6260','ECX6263','ECI6267','ECI5161','ECI5267','ECI5266','MPZ4160'];

$client = getClient();
$service = new Google_Service_Calendar($client);
$service->calendars->clear('primary');

$nodeValues = $crawler->filter('tr')->each(function ($node, $i) use (&$schedules, $enrolments, $service) {

    printf('...');

    $nodeText = cleanNodeText($node->text());
    $isDate = strtotime($nodeText);
    if ($isDate){
        $schedules[$nodeText] = [];
    } else {
        end($schedules);
        $date = key($schedules);
            $attributes = $node->filter('td')->extract(array('_text'));

        if (in_array(cleanNodeText($attributes[0]), $enrolments)) {
            $event['course_code'] = cleanNodeText($attributes[0]);
            $event['description'] = cleanNodeText($attributes[3]);
            $event_time = explode('-', cleanNodeText($attributes[4]));
            $event['from'] = dateInISO8601($date, $event_time[0]);
            $event['to'] = dateInISO8601($date, $event_time[1]);
            $event['color'] = setEventColor($attributes[3]);
            $schedules[$date][] = $event;
            addEventWithRemainder($service, $event);
        }

    }

});


echo "Done";

function cleanNodeText($text)
{
    return  trim(ereg_replace("[[:cntrl:]]", "", $text));
}

function setEventColor($event) {
    if (strpos($event, 'Day School') !== false) {
        return "10";
    }

    if (strpos($event, 'Tutor Marked Assignment') !== false) {
        return "4";
    }

    if (strpos($event, 'Case Study') !== false) {
        return "4";
    }

    if (strpos($event, 'Online Quiz') !== false) {
        return "5";
    }

    if (strpos($event, 'Practical Session') !== false) {
        return "1";
    }

    if (strpos($event, 'Report') !== false) {
        return "9";
    }

    if (strpos($event, 'CA-Test') !== false) {
        return "6";
    }

    if (strpos($event, 'Presentation') !== false) {
        return "4";
    }

    if (strpos($event, 'Lab') !== false) {
        return "2";
    }

    if (strpos($event, 'Mini Project') !== false) {
        return "6";
    }

    if (strpos($event, 'Final Examination') !== false) {
        return "11";
    }
    return "1";
}

function dateInISO8601($date, $time) {
    return date(DATE_ISO8601, strtotime("$date $time"));
}